package controllers;


import java.math.BigInteger;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.control.Alert.AlertType;
import models.methods;
public class rsa_step2_controller {
	
	@FXML Button btn_encrypt,btn_decrypt,btn_e,btn_d;
	@FXML TextField txt_e,txt_d,msg_encrypt,msg_decrypt;
	@FXML Label label_for_n,label_result;
	
	String text = methods.N+"";
	long e = 0, d = 0 ,msg_e = 0, msg_d = 0;
	String string_txt_e,string_msg_e,string_txt_d,string_msg_d;
	long result = 1;
	//shiko big integer
	public void start1() {
		methods.addTextLimiter(txt_e, 3);
		methods.addTextLimiter(txt_d, 3);
		methods.addTextLimiter(msg_encrypt, 3);
		methods.addTextLimiter(msg_decrypt, 3);
	}
	
	
	public void encrypt() {
		
		try {
			string_txt_e = txt_e.getText().toString();	
			string_msg_e = msg_encrypt.getText().toString();
		
		if(string_txt_e.isEmpty() || string_msg_e.isEmpty()) {
			methods.alerti("Warning ", null, "Please fill out all the fields !", AlertType.WARNING);
			return;
		}
		
		e = Long.parseLong(string_txt_e);
		BigInteger b1 = BigInteger.valueOf(e);
		msg_e = Long.parseLong(string_msg_e);
		
		result = (long) (Math.pow(msg_e, e)%methods.N);
		label_result.setText("Result is : "+result);
		
		}
		catch(NumberFormatException e) {
			
			methods.alerti("Wrong Input", "Error", "You must enter only numbers!", AlertType.ERROR);
		}
	}
	
	public void enable_encrypt() {
		txt_d.setDisable(true);
		msg_decrypt.setDisable(true);
		txt_d.setText("");
		msg_decrypt.setText("");
		label_result.setText("");
		txt_e.setDisable(false);
		msg_encrypt.setDisable(false);
		btn_e.setDisable(false);
		btn_d.setDisable(true);
		label_for_n.setText("Value of N from step 1 is : "+text);
		
	}
	
	
	public void decrypt() {
		
		try {
			string_txt_d = txt_d.getText().toString();	
			string_msg_d = msg_decrypt.getText().toString();
		
		if(string_txt_d.isEmpty() || string_msg_d.isEmpty()) {
			methods.alerti("Warning ", null, "Please fill out all the fields !", AlertType.WARNING);
			return;
		}
		
		d = Long.parseLong(string_txt_d);
		msg_d = Long.parseLong(string_msg_d);
		
		result = (long) (Math.pow(msg_d, d)%methods.N);
		label_result.setText("Result is : "+result);
		
		}
		catch(NumberFormatException e) {
			
			methods.alerti("Wrong Input", "Error", "You must enter only numbers!", AlertType.ERROR);
		}
	
	}
	
	public void enable_decrypt() {
		label_for_n.setText("Value of N from step 1 is : "+text);
		txt_e.setDisable(true);
		msg_encrypt.setDisable(true);
		txt_e.setText("");
		msg_encrypt.setText("");
		label_result.setText("");
		txt_d.setDisable(false);
		msg_decrypt.setDisable(false);
		btn_d.setDisable(false);
		btn_e.setDisable(true);
	}
}
