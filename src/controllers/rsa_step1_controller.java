package controllers;

import java.io.IOException;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import models.methods;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.image.Image;
import javafx.stage.Stage;
import javafx.scene.Node;
public class rsa_step1_controller {

	@FXML private TextField txtP,txtQ;
	@FXML private Button btn_check,btn_reset,btn_find_phi;
	@FXML private Label label_step1,label_n,label_find_phi;
	long nr1 = 0;
	long nr2 = 0;
	String string_txtp = "";
	String string_txtq = "";
	Stage stage;
	
	
	
	public void start1() {
		methods.addTextLimiter(txtP, 5);
		methods.addTextLimiter(txtQ, 5);
	}
	public void check_p_and_q() {
		
		try {	
		
		
		string_txtp = txtP.getText().toString();	
		string_txtq = txtQ.getText().toString();	
		
		if(string_txtp.isEmpty() || string_txtq.isEmpty()) {
			methods.alerti("Warning ", null, "Please fill out all the fields !", AlertType.WARNING);
			return;
		}
		
		nr1 = Long.parseLong(string_txtp);
		nr2 = Long.parseLong(string_txtq);
		methods.N = nr1*nr2;
		if(relativelyPrime(nr1, nr2)) {
			System.out.println("yes");
			label_step1.setText("Yes, They are relatively prime");
			label_n.setText("N is "+nr1*nr2);
			btn_check.setStyle("-fx-background-color: #2e9b31");
			//btn_check.setStyle("-fx-background-image: url('../images/correct.png');");
			btn_find_phi.setDisable(false);
		}
		else {
			System.out.println("no");
			label_step1.setText("NOT relatively prime");
			btn_check.setStyle("-fx-background-color: #e54632");
			label_n.setText("");
		//	btn_check.setStyle("-fx-background-image: url('../images/wrong.png');");
			btn_find_phi.setDisable(true);
		}
		
		}
		catch(NumberFormatException e) {
			
			methods.alerti("Wrong Input", "Error", "You must enter only numbers!", AlertType.ERROR);
		}
	}
	
	private static long gcd(long a, long b) {
		long t;
	    while(b != 0){
	        t = a;
	        a = b;
	        b = t%b;
	    }
	    return a;
	}
	
	
	private static boolean relativelyPrime(long nr12, long nr22) {
	    return gcd(nr12,nr22) == 1;
	}
	
	public void reset_fields() {
		txtP.setText("");
		txtQ.setText("");
		btn_find_phi.setDisable(true);
		label_find_phi.setText("");
		label_step1.setText("");
		label_n.setText("");
		btn_check.setStyle("-fx-background-color: #e54632");
		
	}
	
	
	public void step_2(Event event) throws IOException{
				Parent root;
				
	            root = FXMLLoader.load(getClass().getResource("/view/rsa2.fxml"));
	            Stage stage = new Stage();
	            Scene scene = new Scene(root);
	    		
	            stage.getIcons().add(new Image("/images/icon.png"));
	            stage.setScene(scene);
	            stage.setTitle("RSA Algorithm");
	            stage.setResizable(false);
	            stage.show();
	            
	            System.out.println(methods.N);
	            ((Node)(event.getSource())).getScene().getWindow().hide();
	            
	}
	
	public void find_phi() {
		long p = nr1-1;
		long q = nr2-1;
		
		label_find_phi.setText("Φ(N) is "+(p*q));
	}
	

	
}
